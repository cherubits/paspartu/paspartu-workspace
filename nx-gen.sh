#!/bin/sh

ddd_schematics () {
  local GROUP=$1 
  local DOMAIN=$3
  local ORG=$2
  yarn nx generate @nrwl/react:library pres \
   --unitTestRunner=jest \
   --bundler=rollup \
   --directory=${DOMAIN} \
   --appProject=${DOMAIN}-frontend \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-pres \
   --pascalCaseFiles \
   --publishable \
   --no-routing \
   --no-interactive
  yarn nx generate @nrwl/nest:library domain \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-domain \
   --service \
   --strict \
   --no-interactive 
  yarn nx generate @nrwl/nest:library infra \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-infra \
   --service \
   --strict \
   --no-interactive 
  yarn nx generate @nrwl/nest:library app \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-app \
   --service \
   --strict \
   --no-interactive 
}

ddd_fe () {
  local GROUP=$1 
  local DOMAIN=$3
  local ORG=$2
  yarn nx generate @nrwl/react:application frontend \
   --directory=${DOMAIN} \
   --pascalCaseFiles \
   --routing \
   --no-interactive
  yarn nx generate @nrwl/react:library pres \
   --unitTestRunner=jest \
   --bundler=rollup \
   --directory=${DOMAIN} \
   --appProject=${DOMAIN}-frontend \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-pres \
   --pascalCaseFiles \
   --publishable \
   --no-routing \
   --no-interactive
}

ddd_be () {
  local GROUP=$1 
  local DOMAIN=$3
  local ORG=$2
  yarn nx generate @nrwl/nest:library domain \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-domain \
   --service \
   --strict \
   --no-interactive 
  yarn nx generate @nrwl/nest:library infra \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-infra \
   --service \
   --strict \
   --no-interactive 
  yarn nx generate @nrwl/nest:library app \
   --buildable \
   --publishable \
   --directory=${DOMAIN} \
   --importPath=${GROUP}/${ORG}-${DOMAIN}-app \
   --service \
   --strict \
   --no-interactive 
  yarn nx generate @nrwl/nest:application backend \
   --frontendProject=${DOMAIN}-frontend \
   --directory=${DOMAIN} \
   --no-interactive
}

NPM_ORG=@lordoftheflies
PROJECT=paspartu

ddd_schematics ${NPM_ORG} ${PROJECT} core
ddd_schematics ${NPM_ORG} ${PROJECT} uaac
ddd_schematics ${NPM_ORG} ${PROJECT} etl
ddd_fe ${NPM_ORG} ${PROJECT} alchemyst
ddd_be ${NPM_ORG} ${PROJECT} alchemyst
ddd_fe ${NPM_ORG} ${PROJECT} worksheet
ddd_be ${NPM_ORG} ${PROJECT} worksheet

