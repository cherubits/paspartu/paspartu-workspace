import { Test } from '@nestjs/testing';
import { EtlInfraService } from './etl-infra.service';

describe('EtlInfraService', () => {
  let service: EtlInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [EtlInfraService],
    }).compile();

    service = module.get(EtlInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
