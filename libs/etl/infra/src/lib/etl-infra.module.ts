import { Module } from '@nestjs/common';
import { EtlInfraService } from './etl-infra.service';

@Module({
  controllers: [],
  providers: [EtlInfraService],
  exports: [EtlInfraService],
})
export class EtlInfraModule {}
