import { Test } from '@nestjs/testing';
import { EtlAppService } from './etl-app.service';

describe('EtlAppService', () => {
  let service: EtlAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [EtlAppService],
    }).compile();

    service = module.get(EtlAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
