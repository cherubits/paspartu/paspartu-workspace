import { Module } from '@nestjs/common';
import { EtlAppService } from './etl-app.service';

@Module({
  controllers: [],
  providers: [EtlAppService],
  exports: [EtlAppService],
})
export class EtlAppModule {}
