import { Test } from '@nestjs/testing';
import { EtlDomainService } from './etl-domain.service';

describe('EtlDomainService', () => {
  let service: EtlDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [EtlDomainService],
    }).compile();

    service = module.get(EtlDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
