import { Module } from '@nestjs/common';
import { EtlDomainService } from './etl-domain.service';

@Module({
  controllers: [],
  providers: [EtlDomainService],
  exports: [EtlDomainService],
})
export class EtlDomainModule {}
