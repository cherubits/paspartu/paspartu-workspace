import { Module } from '@nestjs/common';
import { UaacAppService } from './uaac-app.service';

@Module({
  controllers: [],
  providers: [UaacAppService],
  exports: [UaacAppService],
})
export class UaacAppModule {}
