import { Test } from '@nestjs/testing';
import { UaacAppService } from './uaac-app.service';

describe('UaacAppService', () => {
  let service: UaacAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [UaacAppService],
    }).compile();

    service = module.get(UaacAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
