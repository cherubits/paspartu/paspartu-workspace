import { Test } from '@nestjs/testing';
import { UaacDomainService } from './uaac-domain.service';

describe('UaacDomainService', () => {
  let service: UaacDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [UaacDomainService],
    }).compile();

    service = module.get(UaacDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
