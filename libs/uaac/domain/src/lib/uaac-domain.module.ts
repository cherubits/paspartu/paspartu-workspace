import { Module } from '@nestjs/common';
import { UaacDomainService } from './uaac-domain.service';

@Module({
  controllers: [],
  providers: [UaacDomainService],
  exports: [UaacDomainService],
})
export class UaacDomainModule {}
