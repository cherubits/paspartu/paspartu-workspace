import styles from './AlchemystPres.module.css';

/* eslint-disable-next-line */
export interface AlchemystPresProps {}

export function AlchemystPres(props: AlchemystPresProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AlchemystPres!</h1>
    </div>
  );
}

export default AlchemystPres;
