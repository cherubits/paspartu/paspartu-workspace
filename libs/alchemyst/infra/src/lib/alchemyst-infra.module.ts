import { Module } from '@nestjs/common';
import { AlchemystInfraService } from './alchemyst-infra.service';

@Module({
  controllers: [],
  providers: [AlchemystInfraService],
  exports: [AlchemystInfraService],
})
export class AlchemystInfraModule {}
