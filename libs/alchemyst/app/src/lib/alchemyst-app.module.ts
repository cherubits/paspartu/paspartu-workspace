import { Module } from '@nestjs/common';
import { AlchemystAppService } from './alchemyst-app.service';

@Module({
  controllers: [],
  providers: [AlchemystAppService],
  exports: [AlchemystAppService],
})
export class AlchemystAppModule {}
