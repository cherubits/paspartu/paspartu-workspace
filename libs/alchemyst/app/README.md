# alchemyst-app

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build alchemyst-app` to build the library.

## Running unit tests

Run `nx test alchemyst-app` to execute the unit tests via [Jest](https://jestjs.io).
