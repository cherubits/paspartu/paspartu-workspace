import { Module } from '@nestjs/common';
import { WorksheetInfraService } from './worksheet-infra.service';

@Module({
  controllers: [],
  providers: [WorksheetInfraService],
  exports: [WorksheetInfraService],
})
export class WorksheetInfraModule {}
