import { Test } from '@nestjs/testing';
import { WorksheetInfraService } from './worksheet-infra.service';

describe('WorksheetInfraService', () => {
  let service: WorksheetInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [WorksheetInfraService],
    }).compile();

    service = module.get(WorksheetInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
