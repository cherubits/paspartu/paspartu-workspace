import { Test } from '@nestjs/testing';
import { WorksheetAppService } from './worksheet-app.service';

describe('WorksheetAppService', () => {
  let service: WorksheetAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [WorksheetAppService],
    }).compile();

    service = module.get(WorksheetAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
