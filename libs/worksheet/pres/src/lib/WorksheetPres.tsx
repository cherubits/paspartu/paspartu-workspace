import styles from './WorksheetPres.module.css';

/* eslint-disable-next-line */
export interface WorksheetPresProps {}

export function WorksheetPres(props: WorksheetPresProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to WorksheetPres!</h1>
    </div>
  );
}

export default WorksheetPres;
