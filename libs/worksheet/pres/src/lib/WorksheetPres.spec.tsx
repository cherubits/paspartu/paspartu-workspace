import { render } from '@testing-library/react';

import WorksheetPres from './WorksheetPres';

describe('WorksheetPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<WorksheetPres />);
    expect(baseElement).toBeTruthy();
  });
});
