import { Test } from '@nestjs/testing';
import { CoreInfraService } from './core-infra.service';

describe('CoreInfraService', () => {
  let service: CoreInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CoreInfraService],
    }).compile();

    service = module.get(CoreInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
