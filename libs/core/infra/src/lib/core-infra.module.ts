import { Module } from '@nestjs/common';
import { CoreInfraService } from './core-infra.service';

@Module({
  controllers: [],
  providers: [CoreInfraService],
  exports: [CoreInfraService],
})
export class CoreInfraModule {}
