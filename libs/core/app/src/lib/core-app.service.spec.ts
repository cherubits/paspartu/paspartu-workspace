import { Test } from '@nestjs/testing';
import { CoreAppService } from './core-app.service';

describe('CoreAppService', () => {
  let service: CoreAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CoreAppService],
    }).compile();

    service = module.get(CoreAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
