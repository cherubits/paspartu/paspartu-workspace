import { Module } from '@nestjs/common';
import { CoreAppService } from './core-app.service';

@Module({
  controllers: [],
  providers: [CoreAppService],
  exports: [CoreAppService],
})
export class CoreAppModule {}
