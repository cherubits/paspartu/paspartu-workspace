import { Test } from '@nestjs/testing';
import { CoreDomainService } from './core-domain.service';

describe('CoreDomainService', () => {
  let service: CoreDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [CoreDomainService],
    }).compile();

    service = module.get(CoreDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
